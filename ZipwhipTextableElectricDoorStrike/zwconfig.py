# This class reads the config for Zipwhip
import ConfigParser

class ZwConfig:
	def __init__(self, 
				logger = None,
				clientid = None, sessionid = None,
				):
		# define properties
		self.log = logger
		#self.clientid = clientid
		#self.sessionid = sessionid
		#log.info("ClientId=%s" % clientid)

		self.config = ConfigParser.RawConfigParser()
		self.config.read('/opt/zipwhip/zipwhip.conf')

		# getfloat() raises an exception if the value is not a float
		# getint() and getboolean() also do this for their respective types
		self.acctname = self.config.get('Settings', 'acctname')
		self.phonenum = self.config.get('Settings', 'phonenum')
		self.password = self.config.get('Settings', 'password')
		self.sessionid = self.config.get('Settings', 'sessionid')
		self.clientid = self.config.get('Settings', 'clientid')
		self.log.info("Using zwconfig. Settings read from zipwhip.cfg are acctname=%s, phonenum=%s, pass=%s, sessionid=%s, clientid=%s" %
			(self.acctname, self.phonenum, self.password, self.sessionid, self.clientid))

	def write(self):
		self.config.set('Settings', 'acctname', self.acctname)
		self.config.set('Settings', 'phonenum', self.phonenum)
		self.config.set('Settings', 'password', self.password)
		self.config.set('Settings', 'sessionid', self.sessionid)
		self.config.set('Settings', 'clientid', self.clientid)
		with open('zipwhip.cfg', 'wb') as configfile:
			self.config.write(configfile)
		self.log.info("Writing to config: acctname=%s, phonenum=%s, pass=%s, sessionid=%s, clientid=%s" %
			(self.acctname, self.phonenum, self.password, self.sessionid, self.clientid))
