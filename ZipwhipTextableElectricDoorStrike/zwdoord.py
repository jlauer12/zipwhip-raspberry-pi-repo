#!/usr/bin/python -O
# Zipwhip Text-A-Door
# To kick off the script, run the following from the python directory:
#   PYTHONPATH=`pwd` python testdaemon.py start

# Standard imports
import signal
import os
import sys
import json
import time
import logging
import RPi.GPIO as GPIO
import re
import urllib
import urllib2
import daemon
import lockfile

# Imports from Zipwhip-Lib python library
#lib_path = os.path.abspath('../ZipwhipLib')
#sys.path.append(lib_path)
import zwwebsocket
import zwconfig

# Import classes for this specific project
import zwdoorsqlstats

#third party libs
from daemon import runner

global RELAY_PIN
RELAY_PIN = 3

class App():
    
	def __init__(self, logger):
		self.stdin_path = '/dev/null'
		self.stdout_path = '/opt/zipwhip/log/zipwhip-door.daemon'
		self.stderr_path = '/opt/zipwhip/log/zipwhip-door.daemon'
		self.pidfile_path =  '/var/run/zipwhip-door.pid'
		self.pidfile_timeout = 5
	    
	    
		
		#global COFFEE_SINGLE_PIN, COFFEE_DOUBLE_PIN
		#COFFEE_SINGLE_PIN = 24
		#COFFEE_DOUBLE_PIN = 23 # this is now disabled due to button not working.
		# Don't want relay on, so low state
		#GPIO.setup(3, GPIO.OUT) 
		#GPIO.output(3, GPIO.LOW)
		#global RELAY_PIN
		#RELAY_PIN = 3
		self.gpio_setup()
		#self.gpio_clean()
		
		self.stats = zwdoorsqlstats.ZwStats(logger=logger)
		#stats.incrUserStats(phonenum="3134147502", name="John Lauer", 
		#	coffeesinglecnt=0, coffeedoublecnt=1)
		#stats.printAll()
		
		# get our config info. this is a config file that contains
		# acct phone number, acct name, sessionid, clientid, etc
		self.conf = zwconfig.ZwConfig(logger=logger)
	    
	def run(self):
		while True:
			#Main code goes here ...

			#print "In main program while loop"
			logger.info("In main program loop. Starting socket connection to Zipwhip.")
			#body = "coffee do"
			#if coffee_double_search(body):
			#	push_coffee_double_btn()
			#elif coffee_search(body):
			#	push_coffee_btn()
			#if coffee_search("313-341-4124: coffee\n Sent via Zipwhip"):
			#	print "cool. found the word coffee"
			#elif coffee_double_search("coffee double\nSent via Zipwhip"):
			#	print "cool. found coffee double"
			logger.info("Connecting to Zipwhip for account phone number %s" % self.conf.phonenum)
			zwclient = zwwebsocket.ZwWebSocket(showframes=True, sessionid=self.conf.sessionid, clientid=self.conf.clientid)
			#zwclient.clientid = CLIENTID
			zwclient.on_new_message = self.on_new_msg
			self.stats.printAll()
			zwclient.connect()
			logger.info("Reconnecting in 10 seconds")
			time.sleep(10)
			
			#Note that logger level needs to be set to logging.DEBUG before this shows up in the logs 
			logger.debug("Debug message") 
			logger.info("Info message") 
			logger.warn("Warning message")
			logger.error("Error message")
			time.sleep(10)

	def gpio_setup(self):
		# Don't want relay on, so low state
		global RELAY_PIN
		# to use Raspberry Pi board pin numbers
		#GPIO.setmode(GPIO.BOARD)
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(RELAY_PIN, GPIO.OUT) 
		GPIO.output(RELAY_PIN, GPIO.LOW)
		#GPIO.setup(COFFEE_SINGLE_PIN, GPIO.OUT)
		#GPIO.setup(COFFEE_DOUBLE_PIN, GPIO.OUT)
		logger.info("Did GPIO setup for RELAY_PIN: %s" % RELAY_PIN)
	
	def gpio_clean(self):
		GPIO.cleanup()
		logger.info("Did GPIO cleanup RELAY_PIN: %s" % RELAY_PIN)
	
	def trigger_relay(self):
		#self.gpio_setup()
		logger.info("Triggering relay to open door on RELAY_PIN: %s" % RELAY_PIN)
		# turn on the relay for 60 seconds
		#global RELAY_PIN
		GPIO.output(RELAY_PIN, GPIO.HIGH)
		time.sleep(60)
		GPIO.output(RELAY_PIN, GPIO.LOW)
		#self.gpio_clean()
		logger.info("Done triggering relay.")

	def send_msg(self, to, msg):
		url = 'http://network.zipwhip.com/message/send'
		values = {"session" : self.conf.sessionid,
				"contacts" : to,
				"body" : msg,
				"fromAddress" : "0",
				"fromName" : "",
				"scheduledDate" : "-1" }
	
		data = urllib.urlencode(values)
		logger.info("Sending POST data to url: " + url)
		req = urllib2.Request(url, data)
		response = urllib2.urlopen(req)
		the_page = response.read()
		logger.info("Response:")
		logger.info(the_page)
	
	def unlock_search(self, str):
		# search for coffee
		logger.info("Searching for word 'unlock' in '%s'" % str)
		matchObj = re.search( r'^(\s*)(unlock)(\s*)$', str, re.M|re.I)
		if matchObj:
			logger.info("Found the word unlock. Nice!")
		return matchObj
		
	def on_new_msg(self, caller, jsn):
		logger.info("I got myself an on_new_msg")
		if jsn["signal"]["event"] == "receive" :
			logger.info(json.dumps(jsn, sort_keys=True,
				indent=4, separators=(',', ': ')))
			# see if it's the word "coffee double" at the start
			# see if it's the word "coffee"
			body = jsn["signal"]["content"]["body"]
			addr = jsn["signal"]["content"]["address"]
			fname = jsn["signal"]["content"]["firstName"]
			lname = jsn["signal"]["content"]["lastName"]
			if self.unlock_search(body):
				# user ordered a double coffee. do it.
				
				# record some stats
				self.stats.incrUserStats(phonenum=addr, name=fname + " " + lname, 
					unlockcnt=0)
				
				# send back msg that we unlocked the door
				if fname:
					msg = fname + ", You just unlocked Zipwhip's front door. It will stay open for 60 seconds. Made possible through the power of Zipwhip's Cloud Texting."
					if msg.__len__() > 160:
						msg = fname + ", , You just unlocked Zipwhip's front door. It will stay open for 60 seconds."
				else:
					msg = "You just unlocked Zipwhip's front door. It will stay open for 60 seconds. Made possible through the power of Zipwhip's Cloud Texting."
				self.send_msg(addr, msg) 
				# now trigger the relay. this will block for 60 seconds
				self.trigger_relay()
				
		else :
			logger.info("It was a msg of \"event\":\"%s\"" % jsn["signal"]["event"])
	
	def on_new_sig(self, caller, jsn):
		logger.info("Got on_new_sig")
		logger.info(jsn)
	
	

logger = logging.getLogger("DaemonLog")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/opt/zipwhip/log/zipwhip-door.log")
handler.setFormatter(formatter)
logger.addHandler(handler)

app = App(logger=logger)

daemon_runner = runner.DaemonRunner(app)

#This ensures that the logger file handle does not get closed during daemonization
daemon_runner.daemon_context.files_preserve=[handler.stream]
daemon_runner.do_action()


# account parameters
#PHONE_NUM = "3134147502"
#SESSIONID = "462fa4db-c603-4fc3-8bd4-6946e7e861d8:1"
#CLIENTID = "190665185806651392"
# David Diggs Landline
#PHONE_NUM = "2062164915"
#SESSIONID = "77b0eb70-cceb-45b1-8f8c-056362414cb1:189743004"
#CLIENTID = "199760407282978816"

print "Exiting..."
