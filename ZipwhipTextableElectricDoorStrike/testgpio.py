#!/usr/bin/env python
from time import sleep
import RPi.GPIO as GPIO
 
GPIO.setmode(GPIO.BCM)

# We are using pins 8 and 9
# BCM mode Pin 2, BOARD mode Pin 8 INPUT is for the button detect for resetting wifi.
# BCM mode Pin 3, BOARD mode Pin 9 OUTPUT is for sending a voltage to the NPN transistor to turn on the relay

# set up GPIO input with pull-up control
#   (pull_up_down be PUD_OFF, PUD_UP or PUD_DOWN, default PUD_OFF)

# We need pulldown so no float, when btn pushed this input will go high
GPIO.setup(2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
# Don't want relay on, so low state
GPIO.setup(3, GPIO.OUT) 
GPIO.output(3, GPIO.LOW)

relayState = GPIO.LOW

while True:
	#print "GPIO.input(2) = "
	print GPIO.input(2)
	#if ( GPIO.input(2) == False ):
	#	print "Btn is false"
	#else:
	#	print "Btn is true"
		
	# turn on relay
	# set RPi board pin 9 high
	
	if (relayState == GPIO.LOW):
		relayState = GPIO.HIGH
		GPIO.output(3, GPIO.HIGH)
		print "Just set relay to high"
	else:
		relayState = GPIO.LOW
		GPIO.output(3, GPIO.LOW)
		print "Just set relay to low"
	
	sleep(10)
