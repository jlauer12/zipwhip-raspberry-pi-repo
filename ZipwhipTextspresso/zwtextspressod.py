#!/usr/bin/python -O
#
# Zipwhip Textspresso Mini
#
# This daemon connects to the Zipwhip network using the configured account
# from the web interface. That account info is written to /opt/zipwhip/conf/zipwhip.conf
# This process then watches all of the incoming messages for the exact coffee commands.
# This daemon also reads the GPIO ports to understand what LED's are turned on
# in front of the espresso machine to know it's state. It also sends out commands via
# the GPIO ports to trigger the on/off, coffee, decalcify, and water buttons.
# The pre-ground and coffee double buttons are not available because they were left
# as manual touch buttons on the front of the machine to allow for the resetting
# of the Raspi into Wifi Ad-Hoc mode if the pre-ground button is held down for 10 seconds.
#
# To kick off the script, run the following from the python directory:
#   PYTHONPATH=`pwd` python testdaemon.py start

import pdb

# Standard imports
import signal
import os
import sys
import json
import time
import logging
import re
import urllib
import urllib2
import daemon
import lockfile

# Third party libs
from daemon import runner

class App():
    
	def __init__(self, logger=None):
		self.stdin_path = '/dev/null'
		self.stdout_path = '/opt/zipwhip/log/zipwhip-textspresso.daemon_stdout'
		self.stderr_path = '/opt/zipwhip/log/zipwhip-textspresso.daemon_stderr'
		self.pidfile_path =  '/var/run/zipwhip-textspresso.pid'
		self.pidfile_timeout = 5
		
		self.log = logger
		
		# our global queue of previous commands
		self.pastcmds = []
	    
	def run(self):
		
		# Import other Zipwhip libraries
		import zwwebsocket
		import zwconfig
		import zwsendbtncmds
		import zwmsghandler
		import zwreadstate
		import zwsqlstats
		
		self.stats = zwsqlstats.ZwStats()
		
		# get our config info. this is a config file that contains
		# acct phone number, acct name, sessionid, clientid, etc
		self.conf = zwconfig.ZwConfig(logger=logger)
		
		# create our msg handler object for parsing, composing
		self.zwmsg = zwmsghandler.ZwMsgHandler(stats=self.stats, conf=self.conf)
		
		# get our obj to send button commands
		# btw, this turns on GPIO 9 and 10 to HIGH which is critical
		# to do as QUICKLY as possible or the Espresso machine thinks
		# all 4 buttons are being pressed at the start including on/off and coffee
		self.sendbtncmds = zwsendbtncmds.ZwSendBtnCmds()
		
		# get our obj to read the LED state
		self.ledstate = zwreadstate.ZwReadState()
		
		zwclient = zwwebsocket.ZwWebSocket(showframes=True, sessionid=self.conf.sessionid, clientid=self.conf.clientid)
		zwclient.on_new_message = self.on_new_msg

		while True:
			#Main code goes here ...

			#print "In main program while loop"
			self.log.info("In main program loop. Starting socket connection to Zipwhip.")
			self.log.info("Connecting to Zipwhip for account phone number %s" % self.conf.phonenum)
			zwclient.connect()
				
			# if we get here, we need to cleanup all threads that zwclient created
			# so when we loop we don't have multiple threads being created
			
			self.log.info("Reconnecting in 10 seconds")
			time.sleep(10)
			
			#Note that logger level needs to be set to logging.DEBUG before this shows up in the logs 
			self.log.debug("Debug message") 
			self.log.info("Info message") 
			self.log.warn("Warning message")
			self.log.error("Error message")
			time.sleep(10)
			
	def on_new_msg(self, caller, jsn):

		self.log.info("I got myself an on_new_msg")
		
		if jsn["signal"]["event"] == "receive" :
			
			self.log.info(json.dumps(jsn, sort_keys=True,
				indent=4, separators=(',', ': ')))
			body = jsn["signal"]["content"]["body"]
			addr = jsn["signal"]["content"]["address"]
			fname = jsn["signal"]["content"]["firstName"]
			lname = jsn["signal"]["content"]["lastName"]

			#pdb.set_trace()
			
			# use our handy utility to parse to a cmd
			# RETURNS: coffee, coffeeSingle, coffeeTriple, water, decalcify, help, stop, menu, off, on, status
			cmd = self.zwmsg.parseMsg(body)
			self.log.info("Cmd back in main Textspresso loop: " + cmd)
			
			if (cmd == "menu"):
				
				msg = "Welcome to Zipwhip's Textspresso. You can send the following commands:\n\ncoffee\ncoffee single\nthe zipwhip (triple shot)\n\nSend help for more commands."
				self.zwmsg.sendMsg(addr, msg)
				self.log.info("Sent msg back")
				
			elif (cmd == "help"):
				
				msg = "Send the word menu to get the menu for the drinks this machine dispenses. Send the word decalcify to mimic pressing the decalcification button."
				self.zwmsg.sendMsg(addr, msg)
				msg = "You can also send:\n\n on (turn machine on)\noff (turn off)\nstatus (to get state of machine)"
				self.zwmsg.sendMsg(addr, msg)
			elif (cmd == "stop"):
				
				msg = "We support the STOP keyword because of course we're MMA best-practices compliant. However, we never send any texts back so STOP really does nothing."
				self.zwmsg.sendMsg(addr, msg)
				
			elif (cmd == "off"):
				
				isOff = self.ledstate.analyzeIsMachineOff()
				if (isOff):
					msg = "The machine already seems to be off. You're all set."
				else:
					msg = "The machine was on, so we'll turn it off for you."
					# the machine is on, turn it off
					self.sendbtncmds.pushOnOffBtn()
					self.log.info("Sleeping after pushing btn cuz seem to get contention with analysis area")
					#sleep(4)
					
				self.zwmsg.sendMsg(addr, msg)
			
			elif (cmd == "on"):
				
				isOff = self.ledstate.analyzeIsMachineOff()
				isWarmingUp = self.ledstate.analyzeIsWarmingUp()
				err = self.ledstate.getError(fname)
				if (isOff):
					msg = "The machine was off, so we'll turn it on for you so it's warm and ready."
					# the machine is on, turn it off
					self.sendbtncmds.pushOnOffBtn()
					# sleep 4 seconds to get the machine to get going with blinking
					# it's led's so when we check if it's warming up we'll get an accurate read
					#sleep(4)
				elif (isWarmingUp):
					msg = "The machine is already on, but it's warming up."
				elif (err["isErr"]):
					msg = "The machine is on, but it has an error condition. Maybe just out of water or beans."
				else:
					msg = "The machine was on, so you're all set. Already on."
					
				self.zwmsg.sendMsg(addr, msg)
				
				# now, watch for it warming up
				# we should loop here the whole time. roughly 20 loops maybe?
				while(self.ledstate.analyzeIsWarmingUp()):
					isWarmingUp = True
					self.log.info("Warming up...")
					
				if (isWarmingUp):
					msg = "The machine is done warming up. You're set to go."
					self.zwmsg.sendMsg(addr, msg)
					
			elif (cmd == "status"):
				
				isOff = self.ledstate.analyzeIsMachineOff()
				isWarmingUp = self.ledstate.analyzeIsWarmingUp()
				err = self.ledstate.getError(fname)
				if (isOff):
					msg = "The machine is off."		
				elif (isWarmingUp):
					msg = "The machine is warming up."
				elif (err["isErr"]):
					msg = err['msgErr']
				else:
					msg = "The machine is on and ready to go."
					
				# add a tag
				msg += " " + self.zwmsg.getWeather()
				
				# send text back
				self.zwmsg.sendMsg(addr, msg)
				
				self.log.info("Sent text back to user %s, msg: %s" % (addr, msg))
				
			elif (cmd == "coffee" or cmd == "coffeeSingle" or cmd == "coffeeTriple" or cmd == "water"):
				
				# keep track of shots we were able to get out
				shotsVended = 0
				
				# record start time to measure how long it takes to brew
				startBrewTime = time.time()
				
				# send back msg that we are about to vend and how long it'll take
				self.log.info("about to create a msg")
				msgOut = self.zwmsg.createMsg(cmd, addr, fname, lname)
				
				self.log.info("The msg we will send is: %s" % msgOut)
				
				# send text back
				self.zwmsg.sendMsg(addr, msgOut)
				
				self.log.info("Sent text back to user %s." % addr)
				
				# do a pre-analysis of the state
				#isOff = self.ledstate.analyzeIsMachineOff()
				#isWarmingUp = self.ledstate.analyzeIsWarmingUp()
				#err = self.ledstate.getError(fname)
				
				
				# This will send the coffee command to the machine
				# RETURNS err dictionary based on what happened
				self.log.info("About to enter our problem area of sendCoffee()")
				err = self.sendCoffee(cmd, fname)
				self.log.info("Just returned from send coffee. Do we have an err?")
				
				if (err["isErr"]):
					
					self.log.info("We do have an error")
					
					# ok, we got an error. we have a nice msg composed for us on what the error is.
					msgErr = err["msgErr"]
					if (cmd == "coffee" or cmd == "coffeeTriple"):
						# we're not done vending yet, so let them know only the 1st shot was made
						# if beans out tho no shot was made
						# if water out then shot was made, but ended with err
						if (err["isBeansOut"]):
							msgErr = "Didn't get any of ur coffee made. %s" % msgErr
						else:
							msgErr = "We got 1 shot of your coffee made, but got an err. %s" % msgErr
						
					self.zwmsg.sendMsg(addr, msgErr)
					self.log.info("The err msg we sent was: %s" % msgErr)
					
					# log the order
					# self, phonenum, cmd, fname="", lname="", msgin="", msgout="", msgout2=""
					#	beansout=0, waterout=0, trayneededemptied=0
					self.stats.logOrder(addr, cmd, fname, lname, body, msgOut, msgErr, err["isBeansOut"], err["isWaterOut"], err["isTrayNeededEmptied"])
					
					self.log.info("Ok, logged order with error and sent msg back.")
				
				else:
					
					self.log.info("We do not have an error from the sendCofffee. That's great.")
					
					shotsVended += 1
					
					# success. well not quite yet. if it was coffee (which is a double)
					# we have to send one more command to be done.
					# so switch to a coffeeSingle this go around
					
					if (cmd == "coffee" or cmd == "coffeeTriple"):
						
						err = self.sendCoffee("coffeeSingle", fname)
						
						if (err["isErr"]):
						
							# ok, we got an error. we have a nice msg composed for us on what the error is.
							msgErr = err["msgErr"]
							if (cmd == "coffeeTriple"):
								# we're not done vending yet, so let them know only the 1st shot was made
								# if beans out tho no shot was made
								# if water out then shot was made, but ended with err
								if (err["isBeansOut"]):
									msgErr = "We got 1 shot of ur coffee made, but got err. %s" % msgErr
								else:
									msgErr = "We got 2 shots of ur coffee made, but got err. %s" % msgErr
									shotsVended += 1
							
							if (cmd == "coffee"):
								if (err["isBeansOut"]):
									msgErr = "Your coffee is all set. FYI, we ran out of beans. When u pick up ur coffee can u refill me?"
									shotsVended += 1
								if (err["isWaterOut"]):
									msgErr = "Your coffee is done. FYI, we ran out of water at the end. Can u refill me for the next person?"
									shotsVended += 1
								# otherwise, on other items, send the error
								
							self.zwmsg.sendMsg(addr, msgErr)
							self.log.info("The err msg we sent was: %s" % msgErr)
							
							# log the order
							# self, phonenum, cmd, fname="", lname="", msgin="", msgout="", msgout2=""
							#	beansout=0, waterout=0, trayneededemptied=0
							self.stats.logOrder(addr, cmd, fname, lname, body, msgOut, msgErr, err["isBeansOut"], err["isWaterOut"], err["isTrayNeededEmptied"])
						
						else:
							
							# we may be done if it's a coffee (which is a double)
							shotsVended += 1
							
							# in case it was a coffee triple
							if (cmd == "coffeeTriple"):
						
								# send the last shot
								err = self.sendCoffee("coffeeSingle", fname)
								
								if (err["isErr"]):
								
									# ok, we got an error. we have a nice msg composed for us on what the error is.
									msgErr = err["msgErr"]
									if (cmd == "coffeeTriple"):
										# we're not done vending yet, so let them know only the 1st shot was made
										# if beans out tho no shot was made
										# if water out then shot was made, but ended with err
										if (err["isBeansOut"]):
											msgErr = "We got 2 shots of ur coffee made, but got err. %s" % msgErr
										else:
											msgErr = "We got 3 shots of ur coffee made, but got err. %s" % msgErr
									
									self.zwmsg.sendMsg(addr, msgErr)
									self.log.info("The err msg we sent was: %s" % msgErr)
									
									# log the order
									# self, phonenum, cmd, fname="", lname="", msgin="", msgout="", msgout2=""
									#	beansout=0, waterout=0, trayneededemptied=0
									self.stats.logOrder(addr, cmd, fname, lname, body, msgOut, msgErr, err["isBeansOut"], err["isWaterOut"], err["isTrayNeededEmptied"])
								
								else:
									
									# Full success on coffee triple!!!!
									elapsed = int(time.time() - startBrewTime)
									msgOut2 = "Your \"Zipwhip\" specialty drink is brewed. It took me %s to whip it up for u. Enjoy."	% self.zwmsg.humanize_time(elapsed)
									self.zwmsg.sendMsg(addr, msgOut2)
									self.stats.logOrder(addr, cmd, fname, lname, body, msgOut, msgOut2)
							else:
								
								# Full success on coffee (double)!!!!
								elapsed = int(time.time() - startBrewTime)
								msgOut2 = "Your coffee is brewed. It took me %s to whip it up for u. Enjoy." % self.zwmsg.humanize_time(elapsed)
								self.zwmsg.sendMsg(addr, msgOut2)
								self.stats.logOrder(addr, cmd, fname, lname, body, msgOut, msgOut2)
					
					else:
						
						# We get here if vended coffee on first try and had all successes
						
						self.log.info("We're done. Yeah baby. Coffee vended.")
						# Full success on coffee single!!!!
						elapsed = int(time.time() - startBrewTime)
						msgOut2 = "Your coffee single is brewed. It took me %s to whip it up for u. Enjoy." % self.zwmsg.humanize_time(elapsed)
						self.zwmsg.sendMsg(addr, msgOut2)
						self.stats.logOrder(addr, cmd, fname, lname, body, msgOut, msgOut2)
				
				
			# push onto the queue what our last command was, just to have a record
			self.pastcmds.append({"cmd":cmd,"addr":addr,"fname":fname,"lname":lname,"time":time.strftime('%Y-%m-%d %H:%M:%S')})
			self.log.info("Pastcmds storage list:")
			self.log.info(self.pastcmds)
			
		else :
			self.log.info("It was a msg of \"event\":\"%s\"" % jsn["signal"]["event"])


	def sendCoffee(self, cmd, fname):
		#pdb.set_trace()
		self.log.info("Inside sendCoffee. Going to analyze if machine is off.")
		
		# This will send the coffee command
		# first of all, see if machine is off. if so, turn it on.
		isOff = self.ledstate.analyzeIsMachineOff()
		# we can call this cuz we just did an analysis
		#isOff = self.ledstate.isMachineOff()
		#self.ledstate.startReadingState()
		#self.ledstate.printData()
		#self.ledstate.analyzeData()
		#isOff = isMachineOff()
		self.log.info("Done analyzing if machine is off. isOff: %s" % isOff)
		
		if (isOff):
			
			self.log.info("Apparently we're not on right now. Turning machine on...")
			
			# the machine is off, turn it on. this btn push is about 2 seconds long.
			self.sendbtncmds.pushOnOffBtn()
			
		else:
			self.log.info("Actually, looks like we're on. That's great")
			
		# now, we should wait a bit cuz we know this takes a bit to warm up
		# we have to loop watching to see if we get two solid lights
		self.log.info("Going to enter scary while loop while we watch for warming up...")
		while (self.ledstate.analyzeIsWarmingUp()):
			
			# we'll loop here until we get something other than a warming up situation
			self.log.info("Warming up...")
			
		# WE KNOW WE'RE ON NOW. COOL.
		# ok, if we get this far, we've warmed up. hopefully we're ready to brew.
		# however, we may be in an error state.
		
		#if (self.ledstate.analyzeIsReadyToBrew()):
			
			# for now we will blow this off because a red light could be on, but we
			# can still send the coffee command. so, instead, let's just send the coffee
			# command and then check if the isVending is occurring. if there
			# is a hard error, that will return immediately after seeing it's not vending.
			# then we can look for errors and send a response.
			#pass
			
		# we are ready to brew. so lets send the button command.
		if (cmd == "coffee" or cmd == "coffeeSingle" or cmd == "coffeeTriple"):
			
			self.sendbtncmds.pushCofBtn()
			
			# ok, now this takes a bit to vend. watch the blinking leds
			while (self.ledstate.analyzeIsVendingCof()):
				
				self.log.info("Vending coffee...")
			
			# try one more time. it seems the machine doesn't register a vend signal
			# quite that fast
			# ok, now this takes a bit to vend. watch the blinking leds
			#sleep(2)
			while (self.ledstate.analyzeIsVendingCof()):
				
				self.log.info("Vending coffee...")
			
		# if we get here we're done vending
		# see if there are errors. Pass in firstname cuz we get a nicely formatted
		# err back with the user's name if there was one.
		# RETURNS: dictionary with msgErr, isWaterOut, isBeansOut, isTrayNeededEmptied
		self.log.info("About to do analyzeGetError()")
		err = self.ledstate.analyzeGetError(fname)
		
		#if (len(err["msgErr"]) > 0):
			# uh oh, there was an error. just return the err.
		self.log.info("Returning from sendCoffee()")
		return err		
	
	def on_new_sig(self, caller, jsn):
		self.log.info("Got on_new_sig")
		self.log.info(jsn)
	
	

logger = logging.getLogger("zwtextspressod")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/opt/zipwhip/log/zipwhip-textspresso.log")
handler.setFormatter(formatter)
logger.addHandler(handler)
log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=log_format, level=logging.INFO)

app = App(logger=logger)
#app.run()

daemon_runner = runner.DaemonRunner(app)

#This ensures that the logger file handle does not get closed during daemonization
daemon_runner.daemon_context.files_preserve=[handler.stream]
daemon_runner.do_action()

# This never gets hit cuz main daemon run() blocks
print "Exiting Zipwhip Textspresso Daemon..."

