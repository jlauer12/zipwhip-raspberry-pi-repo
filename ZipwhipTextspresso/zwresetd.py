#!/usr/bin/python -O
# Zipwhip Ad-Hoc Wifi Reset Button
# Runs the daemon process that attaches to GPIO11 to watch for the pre-ground 
# button to be pressed for 10 seconds to trigger a reconfig into Ad-Hoc mode 
# for the WLAN0 module.
# To kick off the script, run the following from the python directory:
#   PYTHONPATH=`pwd` python zwresetd.py start

# Standard imports
import signal
import os
import sys
import time
import logging
import daemon
import lockfile
#import RPIO

#third party libs
from daemon import runner

#from zwresetd import on_gpio11_rising

# make our callback a function call to support RPIO's method of callback
#def callback(gpio_id, value):
#	global app
#	app.on_gpio11_rising(gpio_id, value)

class App():
    
	def __init__(self, logger):
		self.stdin_path = '/dev/null'
		self.stdout_path = '/opt/zipwhip/log/zipwhip-adhoc-reset.daemon'
		self.stderr_path = '/opt/zipwhip/log/zipwhip-adhoc-reset.daemon'
		self.pidfile_path =  '/var/run/zipwhip-adhoc-reset.pid'
		self.pidfile_timeout = 5
		#self.user = "root"
		#self.pwd = "/opt/zipwhip"
	    
	    # globals inside class
		self.pinBtn = 11 # pre-ground btn pin on GPIO to read state from opt-coupler
		self.lastWrite = 0
		self.ctr = 0
		self.isJustDidAReset = 0

		#self.gpio_setup()
		#self.gpio_setup()
		#self.rpio = RPIO.Interruptor()
		#self.rpio = RPIO._rpio()
		#self.rpio.setmode(RPIO.BCM)
		#RPIO.setmode(RPIO.BCM)

		# Setup GPIO11 for input of pre-ground button 
		# (coffee double button will be removed from front panel, so can't use 
		# it even though it would work)
		#RPIO.setup(self.pinBtn, RPIO.IN, pull_up_down=RPIO.PUD_OFF)	# pre-ground button
		#logger.info("Did GPIO setup for pre-ground pin: %s" % self.pinBtn)
		
		# Wrap function back into a proper static method
		#self.gpio_callback = staticmethod(self.on_gpio11_rising)
		#self.rpio.add_interrupt_callback(self.pinBtn, self.gpio_callback, edge='rising', threaded_callback=False)
		#RPIO.add_interrupt_callback(self.pinBtn, self.on_gpio11_rising, edge='rising', threaded_callback=False)
		#RPIO.add_interrupt_callback(self.pinBtn, callback, edge='rising', threaded_callback=False)
		
			    
	def run(self):
		
		# one-time run setup code
		# need to do the import for RPIO here because it opens file handles
		# when you daemonize it closes all open file handles, so by importing later
		# we get the file handles opened after daemonizing took place
		import RPIO
		RPIO.setmode(RPIO.BCM)
		RPIO.setup(self.pinBtn, RPIO.IN, pull_up_down=RPIO.PUD_OFF)	# pre-ground button
		logger.info("Did GPIO setup for pre-ground pin: %s" % self.pinBtn)
		RPIO.add_interrupt_callback(self.pinBtn, self.on_gpio11_rising, edge='rising', threaded_callback=False)

		while True:
			#Main code goes here ...

			logger.info("In main program loop. Starting to watch for the reset button which is the pre-ground button in lower right corner of front panel.")

			# main 
			logger.debug("Debug message") 
			logger.info("Info message") 
			logger.warn("Warning message")
			logger.error("Error message")
			
			# This will block once called. No lines below are run.
			RPIO.wait_for_interrupts()
			
			# Line above blocks forever
			#Note that logger level needs to be set to logging.DEBUG before this shows up in the logs 
			logger.debug("Debug message") 
			logger.info("Info message") 
			logger.warn("Warning message")
			logger.error("Error message")
			time.sleep(10)

	def gpio_setup(self):
		RPIO.setmode(RPIO.BCM)

		# Setup GPIO11 for input of pre-ground button 
		# (coffee double button will be removed from front panel, so can't use 
		# it even though it would work)
		RPIO.setup(11, RPIO.IN, pull_up_down=RPIO.PUD_OFF)	# pre-ground button
		logger.info("Did GPIO setup for pre-ground pin: %s" % 11)
	
	def gpio_clean(self):
		RPIO.cleanup()
		logger.info("Did GPIO cleanup RELAY_PIN: %s" % RELAY_PIN)

	def on_gpio11_rising(self, gpio_id, value):
				
		# make sure no more than 1000ms has passed since last write
		# if it has then they let go of the button and we need to return
		# our ctr to 0
		now = int(round(time.time() * 1000))
		if (self.lastWrite < now - 1000):
			# see if we just returned from a reset. this could take several seconds
			# and if so i'd like to debounce. so let's go negative on the ctr
			# so that if they keep holding it we won't keep resetting. they
			# have to lift their finger and do it again from scratch
			if (self.isJustDidAReset == 1):
				self.ctr = -30000
				self.isJustDidAReset = 0
				logger.info("Looks like we just returned from an AdHoc reset. I'm debouncing for 1 minute.")
			else:
				# the last write was greater than 1 second ago, reset ctr
				logger.info("Resetting ctr to 0 cuz last write was more than 1s ago.")
				self.ctr = 0
		
		self.ctr = self.ctr + 1
		self.lastWrite = now
		
		# if ctr gets to 5,000 then we had 10,000 ms pass, which is 10 secs
		# that's long enough to know they want a reset. Each rise is 2.08ms.
		if (self.ctr > 5000):
			# reset ctr to negative in case they hold the button really long
			# this will give us time to not keep resetting the wifi device
			# so basically this adds 60 seconds if they keep holding it
			# if they let up on it for 1s we'll reset to 0 anyway
			self.ctr = -30000
			self.resetWifiToAdhoc()
	
	def resetWifiToAdhoc(self):
		
		self.isJustDidAReset = 1
		logger.info("Going to reset Wifi to ad-hoc mode")
		output = os.system('sudo /opt/zipwhip/bin/wifi_adjust.sh reset')
		logger.info(output)


logger = logging.getLogger("zwresetd")
logger.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler("/opt/zipwhip/log/zipwhip-adhoc-reset.log")
handler.setFormatter(formatter)
logger.addHandler(handler)
log_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=log_format, level=logging.INFO)

app = App(logger=logger)
#app.run()

daemon_runner = runner.DaemonRunner(app)

#This ensures that the logger file handle does not get closed during daemonization
daemon_runner.daemon_context.files_preserve=[handler.stream]
daemon_runner.do_action()

# This never gets hit cuz main daemon run() blocks
print "Exiting zwresetd..."
