#!/usr/bin/env python
import RPIO
import time
from time import sleep
import sys
import os

import logging
log_format = '%(levelname)s | %(asctime)-15s | %(message)s'
logging.basicConfig(format=log_format, level=logging.INFO)

RPIO.setmode(RPIO.BCM)

# Setup GPIO11 for input of pre-ground button (coffee double button will be removed)
RPIO.setup(11, RPIO.IN, pull_up_down=RPIO.PUD_OFF)	# pre-ground button

lastWrite = 0
ctr = 0
isJustDidAReset = 0

def on_gpio11_rising(gpio_id, value):
	
	global ctr, lastWrite, isJustDidAReset
		
	# make sure no more than 1000ms has passed since last write
	# if it has then they let go of the button and we need to return
	# our ctr to 0
	now = int(round(time.time() * 1000))
	if (lastWrite < now - 1000):
		# see if we just returned from a reset. this could take several seconds
		# and if so i'd like to debounce. so let's go negative on the ctr
		# so that if they keep holding it we won't keep resetting. they
		# have to lift their finger and do it again from scratch
		if (isJustDidAReset == 1):
			ctr = -60000
			isJustDidAReset = 0
			logging.info("Looks like we just returned from an AdHoc reset. I'm debouncing for 2 minutes.")
		else:
			# the last write was greater than 1 second ago, reset ctr
			logging.info("Resetting ctr to 0 cuz last write was more than 1s ago.")
			ctr = 0
	
	ctr = ctr + 1
	lastWrite = now
	
	# if ctr gets to 5,000 then we had 10,000 ms pass, which is 10 secs
	# that's long enough to know they want a reset. Each rise is 2.08ms.
	if (ctr > 5000):
		# reset ctr to negative in case they hold the button really long
		# this will give us time to not keep resetting the wifi device
		# so basically this adds 60 seconds if they keep holding it
		# if they let up on it for 1s we'll reset to 0 anyway
		ctr = -60000
		resetWifiToAdhoc()

def resetWifiToAdhoc():
	global isJustDidAReset
	isJustDidAReset = 1
	logging.info("Going to reset Wifi to ad-hoc mode")
	output = os.system('sudo /opt/zipwhip/bin/wifi_adjust.sh reset')
	logging.info(output)

RPIO.add_interrupt_callback(11, on_gpio11_rising, edge='rising', threaded_callback=False)

logging.info("Added callback for pin %s to watch for wifi reset." % 11)

try:
	RPIO.wait_for_interrupts()

except KeyboardInterrupt:
	RPIO.cleanup()
