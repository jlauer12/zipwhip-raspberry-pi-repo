import sqlite3 as lite
import logging
from datetime import date

class ZwStats:
	def __init__(self, 
				logger = None,
				):
		# define properties
		self.log = logger
		con = lite.connect('stats.db')
		#self.log.info("test")
		with con:
			cur = con.cursor()    
			cur.execute('SELECT SQLITE_VERSION()')			
			data = cur.fetchone()
			self.log.info("Configuring zwsql stats. Using SQLite version: %s" % data)
			#cur.execute("DROP TABLE IF EXISTS UserStats")
			try:
				cur.execute("""
					CREATE TABLE UserStats(Id INTEGER PRIMARY KEY AUTOINCREMENT, 
					PhoneNum TEXT UNIQUE, Name TEXT, IsDailyStat INT, 
					CoffeeCnt INT, CoffeeSingleCnt INT, CoffeeDoubleCnt INT, 
					MsgsInCnt INT, MsgsOutCnt INT, FirstOrderDate TEXT, LastOrderDate TEXT)
					""")
			except:
				# just skip creating the table
				self.log.info("Found that UserStats table exists. No need to create it.")
				pass
			
			# Create the global stats record
			try:
				cur.execute("""INSERT INTO UserStats
					VALUES(NULL,'GLOBAL','Machine Stats',0,0,0,0,0,0,datetime(),datetime())""")
				lid = cur.lastrowid
				self.log.info("The last Id of the inserted row is %d" % lid)
				cur.execute("SELECT * FROM UserStats")
				rows = cur.fetchall()
				for row in rows:
					self.log.info(row)
			except:
				# do nothing on err for insert
				pass
	
	def incrUserStats(self, phonenum, name=None, 
		coffeesinglecnt=0, coffeedoublecnt=0, isdailystat=0):
		
		self.log.info(phonenum)
		con = lite.connect('stats.db')
		with con:
			con.row_factory = lite.Row
			cur = con.cursor()
			#cur.execute("SELECT * FROM UserStats WHERE PhoneNum LIKE \"" + phonenum + "\"")			
			cur.execute("SELECT * FROM UserStats WHERE PhoneNum LIKE '%s'" % phonenum)			
			data = cur.fetchall()
			if data:
				# a row exists, update it
				pass
			else:
				# a row does not exist, do insert
				cur.execute("""INSERT INTO UserStats
					VALUES(NULL, ?, ?,0, 0,0,0,0,0,datetime(),datetime())""", (phonenum, name))
				lid = cur.lastrowid
				self.log.info("User does not exist yet. Doing insert. PK = %d" % lid)
			
			# we should always be guaranteed to have a user record now
			sql = "SELECT * FROM UserStats WHERE PhoneNum LIKE '%s'" % phonenum
			self.log.info(sql)
			cur.execute(sql)
			data = cur.fetchone()
			#print data
			self.log.info(data)
			
			# increment everything
			d = {}
			for col in data.keys():
				d[col] = data[col]
				self.log.info("Col %s = %s" % (col, d[col]))
				
			if name:
				d["Name"] = name
			d["CoffeeCnt"] += 1
			d["CoffeeSingleCnt"] += coffeesinglecnt
			d["CoffeeDoubleCnt"] += coffeedoublecnt
			d["MsgsInCnt"] += 1
			if coffeesinglecnt > 0:
				d["MsgsOutCnt"] += 1
			else:
				d["MsgsOutCnt"] += 2
			d["IsDailyStat"] = isdailystat
			
			cur.execute("""UPDATE UserStats SET Name=:Name, 
				IsDailyStat=:IsDailyStat,
				CoffeeCnt=:CoffeeCnt,
				CoffeeSingleCnt=:CoffeeSingleCnt,
				CoffeeDoubleCnt=:CoffeeDoubleCnt,
				MsgsInCnt=:MsgsInCnt,
				MsgsOutCnt=:MsgsOutCnt,
				LastOrderDate=datetime()
				WHERE Id=:Id""", d)        
			con.commit()
			self.log.info( "Number of rows updated: %d" % cur.rowcount)
			
			cur.execute("SELECT * FROM UserStats WHERE PhoneNum LIKE '%s'" % phonenum)
			data = cur.fetchone()
			self.log.info( data)
			
		# now, call myself to increment the global stats
		if phonenum != "GLOBAL" and isdailystat == 0:
			
			# update global stats
			self.log.info("Going to increase global count.")
			self.incrUserStats(phonenum="GLOBAL", coffeesinglecnt=coffeesinglecnt,
				coffeedoublecnt=coffeedoublecnt)
				
			# now, call myself with the daily stat setting to just increment for this day
			# only call ourselves if we're not a global stat, i.e. we're a per user phone number stat
			# and if we're not a dailystat, i.e. we're an individual stat
			self.log.info("Going to increase per day count. Calling now...")
			d = date.today()
			dt = d.isoformat()
			self.log.info("The per day cnt PK = " + dt)
			self.incrUserStats(phonenum=dt, name="Daily Stat", coffeesinglecnt=coffeesinglecnt,
				coffeedoublecnt=coffeedoublecnt, isdailystat=1)
	
	def printAll(self):
		con = lite.connect('stats.db')
		with con:
			con.row_factory = None
			cur = con.cursor()
			cur.execute("SELECT * FROM UserStats")
			rows = cur.fetchall()
			for row in rows:
				self.log.info(row)

def test():
	
	# setup logging
	FORMAT = '%(asctime)s %(levelname)s %(message)s'
	logging.basicConfig(format=FORMAT)
	logger = logging.getLogger()
	logger.setLevel(logging.INFO)
	if not logger.handlers:
            logger.addHandler(logging.StreamHandler())
	
	stats = ZwStats(logger=logger)
	stats.incrUserStats(phonenum="3135551234", name="Jed Smith", 
		coffeesinglecnt=0, coffeedoublecnt=1)
	stats.incrUserStats(phonenum="3135551234", name="Jed Smith", 
		coffeesinglecnt=0, coffeedoublecnt=1)
	stats.printAll()
	
#test()